
vim.wo.relativenumber = true
vim.opt.colorcolumn = "80"
vim.opt.wrap = false
vim.opt.scrolloff = 5
vim.opt.spell = true
vim.opt.spelllang = 'en_us'
