---@type ChadrcConfig 
local M = {}
M.ui = {
  theme = 'catppuccin',
  hl_override = {
    Comment = {
      fg = "vibrant_green"
    }
  },
  transparency = true,
  tabufline = {
    enabled = true,
  },
}
M.plugins = 'custom.plugins'
M.mappings = require "custom.mappings"
return M


