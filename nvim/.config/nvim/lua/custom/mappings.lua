local M = {}

M.general = {
  n = {
    ["<C-h>"] = { "<cmd> TmuxNavigateLeft<CR>", "window left" },
    ["<C-l>"] = { "<cmd> TmuxNavigateRight<CR>", "window right" },
    ["<C-j>"] = { "<cmd> TmuxNavigateDown<CR>", "window down" },
    ["<C-k>"] = { "<cmd> TmuxNavigateUp<CR>", "window up" },
  }
}

M.git = {
  n = {
    ["<leader>gg"] = {
      "<cmd>Git<CR>",
      "Git status"
    }
  }
}

M.dap = {
  plugin = true,
  n = {
    ["<leader>db"] = {
      "<cmd> DapToggleBreakpoint<CR>",
      "Toggle breakpoint"
    },
    ["<leader>dus"] = {
      function ()
        local widgets = require("dap.ui.widgets");
        local sidebar = widgets.sidebar(widgets.scopes);
        sidebar.open();
      end,
      "Open debugging sidebar"
    },
  }
}

M.dap_go = {
  plugin = true,
  n = {
    ["<leader>dgt"] = {
      function()
        require('dap-go').debug_test()
      end,
      "Debug go test"
    },
    ["<leader>dgl"] = {
      function()
        require('dap-go').debug_last()
      end,
      "Debug last go test"
    }
  }
}

M.gopher = {
  plugin = true,
  n = {
    ["<leader>gsj"] = {
      "<cmd> GoTagAdd json <CR>",
      "Add json struct tags"
    },
    ["<leader>gsy"] = {
      "<cmd> GoTagAdd yaml <CR>",
      "Add yaml struct tags"
    }
  }
}

M.telescope = {
  n = {
    ["<leader>u"] = {
      "<cmd>Telescope undo<CR>",
      "Telescope undo"
    },
  }
}

M.harpoon = {
  n = {
    ["<leader>a"] = {
      function ()
        require("harpoon.mark").add_file()
      end,
      "Add file"
    },
    ["<C-b>"] = {
      function ()
        require("harpoon.ui").toggle_quick_menu()
      end,
      "Toggle quick menu"
    },
    ["<C-A-h>"] = {
      function ()
        require("harpoon.ui").nav_file(1)
      end,
      "Nav file 1"
    },
    ["<C-A-j>"] = {
      function ()
        require("harpoon.ui").nav_file(2)
      end,
      "Nav file 2"
    },
    ["<C-A-k>"] = {
      function ()
        require("harpoon.ui").nav_file(3)
      end,
      "Nav file 3"
    },
    ["<C-A-l>"] = {
      function ()
        require("harpoon.ui").nav_file(4)
      end,
      "Nav file 4"
    }
  }
}

M.cellularAutomation = {
  n = {
    ["<leader>mr"] = {
      "<cmd>CellularAutomaton make_it_rain<CR>",
      "Making it rain!"
    }
  }
}

M.lsp = {
	n = {
    ["[d"] = {
      function()
        vim.diagnostic.goto_prev { float = { border = "rounded" } }
      end,
      "Goto prev diag",
    },

    ["]d"] = {
      function()
        vim.diagnostic.goto_next { float = { border = "rounded" } }
      end,
      "Goto next diag",
    },

    ["<leader>f"] = {
      function()
        vim.diagnostic.open_float { border = "rounded" }
      end,
      "Floating diagnostic",
    },

    ["<leader>ra"] = {
      function()
        require("nvchad_ui.renamer").open()
      end,
      "LSP rename",
    },

		-- Restating defaults because lsp mappings don't load with nvim-jdtls for some reason
		["gd"] = {
			function()
				vim.lsp.buf.definition()
			end,
      "LSP definition",
		},
    ["gD"] = {
      function()
        vim.lsp.buf.declaration()
      end,
      "LSP declaration",
    },
		["K"] = {
			function()
				vim.lsp.buf.hover()
			end,
      "LSP hover",
		},

    ["<leader>ca"] = {
      function()
        vim.lsp.buf.code_action()
      end,
      "LSP code action",
    },

    ["gr"] = {
      function()
        vim.lsp.buf.references()
      end,
      "LSP references",
    },

    ["gi"] = {
      function()
        vim.lsp.buf.implementation()
      end,
      "LSP implementation",
    },

    ["<leader>ls"] = {
      function()
        vim.lsp.buf.signature_help()
      end,
      "LSP signature help",
    },

    ["<leader>D"] = {
      function()
        vim.lsp.buf.type_definition()
      end,
      "LSP definition type",
    },

    ["<leader>q"] = {
      function()
        vim.diagnostic.setloclist()
      end,
      "Diagnostic setloclist",
    },

    ["<leader>wa"] = {
      function()
        vim.lsp.buf.add_workspace_folder()
      end,
      "Add workspace folder",
    },

    ["<leader>wr"] = {
      function()
        vim.lsp.buf.remove_workspace_folder()
      end,
      "Remove workspace folder",
    },

    ["<leader>wl"] = {
      function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
      end,
      "List workspace folders",
    },
	},
}

return M
