# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="robbyrussell"
#ZSH_THEME="amuse"
#ZSH_THEME="gnzh"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
#COMPLETION_WAITING_DOTS="false"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    pip
    dotenv
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Append this line to ~/.zshrc to enable fzf keybindings for Zsh:
source /usr/share/doc/fzf/examples/key-bindings.zsh
# Append this line to ~/.zshrc to enable fuzzy auto-completion for Zsh:
source /usr/share/doc/fzf/examples/completion.zsh


#export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")
#export PATH=$PATH:$JAVA_HOME/bin
export PATH=$PATH:~/.local/bin
export PATH=$PATH:/opt/idea-IU-212.5457.46/bin
export PATH=$PATH:/opt/jdt-ls/bin
export PATH=$PATH:/usr/sbin
export PATH=$PATH:/opt/maven/bin
export PATH=$PATH:/usr/local/go/bin

export LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/usr/local/lib64:/usr/lib64
export RUSTC_WRAPPER=sccache
export EDITOR=nvim

# Personals
export DOTFILES=$HOME/.dotfiles
export XDG_CONFIG_HOME=$HOME/.config

# User specific aliases and functions
# Shortcuts
alias ssu='sudo su'
alias l='ll -a'
alias zshconfig="vim ~/.zshrc"
alias ohmyzsh="vim ~/.oh-my-zsh"
alias runvm='vagrant up; vagrant ssh; clear'
alias vicmd='firefox https://spin.atomicobject.com/2016/04/19/vim-commands-cheat-sheet/; clear'
alias gcc32='gcc -m32 -no-pie -fno-stack-protector -z execstack'
alias clear_docker='docker ps -a --format {{.Names}} | grep -v arc_local_db | xargs docker stop | xargs docker rm'
alias mvdl='mv ~/Downloads/"`ls ~/Downloads -Art | tail -n 1`" .'
alias dus='du -hc | sort -rh | head -20'
alias bigfile='du -ha . | sort -rh | head -20' #dust is better imo
alias tld='compgen -c | fzf | xargs tldr'
alias cleannode="fd 'node_modules' -HIt d | xargs du -sh | sort -hr | fzf -m --header 'Select which ones to delete' --preview 'cat \$(dirname {2})/package.json' | awk '{print \$2}' | xargs -r rm -rf"
alias cleanlog="fd -IHe log | xargs du -sh | sort -hr | fzf  -m --preview 'tail -n 10 {+2}' | awk '{print \$2}' | xargs -r rm -rf"
# Location
alias gdoc='cd ~/Documents; clear'
alias gopt='cd /opt; clear'
alias gpro='cd ~/Project; clear'
alias gsc='cd ~/Scripts; clear'
alias gdl='cd ~/Downloads; clear'
alias gpui='cur; cd gmop/kernel/pui/; clear'
alias gplnui='cur; cd gmop/components/auxiliary/pln/pln-ui/;clear'
alias vms='cd ~/Project/pacis-devenv/GSC_VMdev; clear'
alias bast='cd ~/Project/pacis-devenv/Bastion; clear'
alias cur='gpro; cd pacis1; clear'
alias gvue='gpro; cd Vue; clear'
alias ggam='gpro; cd Programmation/C++/Games; clear'
alias arc='cd /usr/local/src/arc; clear'
# Keyboards
alias azer='setxkbmap us dvorak'
alias aoeu='setxkbmap be'
# Apps
alias ide='/opt/idea-IC-223.8214.52/bin/idea.sh &>/dev/null; clear'
alias pm='setsid postman &>/dev/null; clear'
alias frz='setsid franz &>/dev/null; clear'
alias krit='flatpak run org.kde.krita &>/dev/null; clear'
alias stereo='pacmd load-module module-alsa-sink device=hw:1,1; clear'
alias cr='cargo run'
alias vim='nvim'

# enable buildkit for docker
export DOCKER_BUILDKIT=1

# eval $(thefuck --alias)

bindkey -s ^f "tmux-sessionizer\n"

restore() {
  sudo cp "$1" "$1.newbak"
  sudo mv "$1.bak" "$1"
  sudo mv "$1.newbak" "$1.bak"
}

newconf() {
  sudo mv "$1" "$1.bak"
  sudo cp "$2" "$1"
}

[ -f "/home/yoki/.ghcup/env" ] && source "/home/yoki/.ghcup/env" # ghcup-env

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
