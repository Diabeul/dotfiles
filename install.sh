#!/usr/bin/env sh

# Install ansible
sudo apt update
sudo apt install pipx
export PIP_CERT="/etc/ssl/certs"
pipx install ansible==2.16.6

ansible-galaxy collection install community.general
ansible-playbook bootsrap.yml -Kv
