local M = {}

M.treesitter = {
  ensure_installed = {
    "vim",
    "lua",
    "python",
    "html",
    "css",
    "javascript",
    "typescript",
    "tsx",
    "c",
    "markdown",
    "markdown_inline",
    "python",
    "java",
    "rust",
    "go",
    "comment",
    "json",
    "dockerfile",
    "yaml",
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<CR>',
      scope_incremental = '<CR>',
      node_incremental = '<TAB>',
      node_decremental = '<S-TAB>',
    },
  },

  highlight = {
    enable = true,
    use_languagetree = true,
  },

  indent = { enable = true },
}

M.mason = {
  ensure_installed = {
    --lua
    "lua-language-server",
    "stylua",

    -- rust
    "rust-analyzer",

    -- python
    "pyright",
    "black",

    -- go
    "gopls",

    -- java
    "jdtls",
    "java-test",
    "java-debug-adapter",

    -- webdev stuff
    "css-lsp",
    "html-lsp",
    "typescript-language-server",
    "deno",
    "prettierd",
    "prisma-language-server",
    "prettier",

    -- sql
    "sqlls",
    "sql-formatter",

    -- docker
    "dockerfile-language-server",

    -- c/cpp stuff
    "clangd",
    "clang-format"
  },
}

-- git support in nvimtree
M.nvimtree = {
  git = {
    enable = true,
  },

  renderer = {
    highlight_git = true,
    icons = {
      show = {
        git = true,
      },
    },
  },
}

return M
