# My dotfiles
Folder used to install all my local configs, to reproduce my work Environment on any linux device.

![screenshot of the terminal](screen.png)*Screenshot*

## How to install
`./install`
or
`./default_install` (Default Environment Variables)

Note: 
- the neovim folder contains the custom part of the [NvChad](https://nvchad.com/) project.
- cava is a sound visualizer in the terminal
- zathura is a better pdf viewer

## How to remove
`./clean-env`


## VERSION 2.0

require python 3.10

### INIT
note: ansible version of apt is older

`sudo apt install pipx`
export PIP_CERT="/etc/ssl/certs"
`pipx install ansibl-core==2.16.6`

`ansible-galaxy collection install community.general`
`ansible-playbook bootstrap.yml -K`
